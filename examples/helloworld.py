from __future__ import print_function

from zkcluster import cmdline
from zkcluster.client import LocalClient
from zkcluster.rpc import RPC, RPCReg


class ListenCmd(cmdline.RAFTCmdMixin, cmdline.ListenCmd):
    def init_server(self, server, cmdline):
        super(ListenCmd, self).init_server(server, cmdline)
        server.speak_rpc(rpc_reg)


class ConsoleCmd(cmdline.RAFTCmdMixin, cmdline.ConsoleCmd):
    def init_console(self, console, cmdline):
        super(ConsoleCmd, self).init_console(console, cmdline)
        console.speak_rpc(rpc_reg)

    def console_connected(self, console):
        super(ConsoleCmd, self).console_connected(console)
        console.output(
            "Connected to service '%s' (%s:%s) - use 'help' for help.",
            console.servicename, console.host, console.port)


class TestCmd(cmdline.Cmd):
    def create_subparser(self, parser, subparsers):
        return subparsers.add_parser(
            "test_connection",
            help="test the connection")

    def setup_arguments(self, parser):
        parser.add_argument(
            "-s", "--service", type=str,
            default="default", help="name of service")

    def go(self, cmdline):
        client = LocalClient.from_config(
            cmdline.config.config_for_servicename('default'), 'default')
        client.rpc_service.speak_rpc(rpc_reg)
        client.connect()
        response = ClientHello('my target', 'my greeting').send(
            client.rpc_service)
        print("got response: %s" % response)
        client.close()


rpc_reg = RPCReg()


@rpc_reg.call('target', 'greeting')
class ClientHello(RPC):
    def receive_request(self, rpc, transport):
        print("got message: ", self)
        return "hi"

if __name__ == '__main__':
    cmdline.CmdLine(
        cmds=[ListenCmd(), ConsoleCmd(), TestCmd()], prog="demo").main()
