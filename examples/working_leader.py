from __future__ import print_function

from zkcluster import cmdline
from zkcluster import rpc
from zkcluster import worker
from zkcluster import server


class FibWorker(worker.LeaderWorker):
    """Do some work server side, but only if we are the raft "leader".

    Otherwise, receive data from whoever the "leader" is for the same
    work.

    We'll calculate fibonacci numbers.

    """

    @worker.LeaderWorker.receiver
    def fib_numbers(self, obj, **kw):
        """A worker data channel.

        Calling this method locally results in both a local call
        as well as a remote call on all peers.  First the data will be
        sent as an async event to all connected peers and will be sent
        to their fib_numbers() function, then the local fib_numbers()
        function here will be called with the same value.

        """
        print("event: %s (remote: %s)" % (obj, kw['_remote']))

        # keep track of the numbers, as they may have come from our own
        # work() function, or they may have come remotely from a peer
        # that's currently the "worker"
        self.numbers = obj

        # broadcast an RPC event to other, non-peer clients with our fib
        # number. filter the server's connections for just the ones that have
        # our "wl_example_console" token in them, which we set up when we first
        # connected
        self.server.broadcast_rpc_to_clients(
            FibMessage(obj),
            lambda server_connection:
                server_connection.memos.get("wl_example_console", False))

    def __init__(self):
        self.numbers = None

    def work(self):
        """The main work function.

        This function is called at server startup for all peers.
        Peers should check the ".keep_working" attribute which determines
        if we are the "leader" that's doing the work.   The .keep_working
        attribute will block while we're not the "leader".

        Keep track of the fib numbers as self.numbers and always use the
        latest value when we are in the "keep_working" loop.

        """
        if self.numbers is None:
            self.numbers = [1, 1]

        while self.keep_working:
            print("starting work")
            while self.continue_work:
                numbers = self.numbers
                if numbers[0] > 100000000:
                    numbers = [1, 1]
                else:
                    numbers = [numbers[1], numbers[0] + numbers[1]]
                self.fib_numbers(numbers)
                self.async_suite.sleep(1)

            print("stopping work")

rpc_reg = rpc.RPCReg()


@rpc_reg.call('numbers')
class FibMessage(rpc.RPCEvent):
    """Define an RPC message we will send from our server to connected
    console clients.

    """
    def receive_request(self, rpc, transport):
        print("Received numbers: %s" % self.numbers)


class ListenCmd(cmdline.RAFTCmdMixin, cmdline.ListenCmd):
    """Set up the --listen command to start a server.

    The server will have RAFT/P2P functionality, will be able to send
    our FibMessage message to consoles, and finally will also run our
    FibWorker() server-side worker.

    """
    def init_server(self, server, cmdline):
        super(ListenCmd, self).init_server(server, cmdline)
        server.speak_rpc(rpc_reg)
        FibWorker().work_for(server)


class ConsoleCmd(cmdline.RAFTCmdMixin, cmdline.ConsoleCmd):
    """Set up the --console command to connect to a cluster."""

    def init_console(self, console, cmdline):
        super(ConsoleCmd, self).init_console(console, cmdline)
        console.speak_rpc(rpc_reg)

    def console_connected(self, console):
        super(ConsoleCmd, self).console_connected(console)

        # we're connected.  Send some data to the server that it will store
        # in the ".memos" dictionary server-side.  Our FibMessage broadcast
        # will check for this attribute before sending a message.
        server.AddMemos({"wl_example_console": True}).send(console.rpc_service)

        console.output(
            "Connected to service '%s' (%s:%s) - use 'help' for help.",
            console.servicename, console.host, console.port)


if __name__ == '__main__':
    cmdline.CmdLine(
        cmds=[ListenCmd(), ConsoleCmd()], prog="working_leader").main()
