"""A P2P "Peer" implementation that rides on top of the
RemoteStatusServer / StatusClient system.

It knows only about sending and receiving data on a TCP socket; other
details as far as connecting, receiving connections, and disconnecting
are handled by the RemoteStatusServer / StatusClient.

"""
from .client import GreenClient
from . import util
from . import rpc
from . import exc

import logging

log = logging.getLogger(__name__)


class PeerRunner(object):
    name = 'p2p'

    def __init__(self, service, is_bootstrap=False):
        self.service = service
        self.rpc_reg = rpc_reg
        self.is_bootstrap = is_bootstrap
        self.nodename = self.service.nodename
        self.peernames = [
            n['name'] for n in self.service.service_config['nodes']]

        service.speak_rpc(rpc_reg)
        service.memos['p2p'] = self
        util.event_listen(service, "before_listen", self.before_listen)
        util.event_listen(service, "on_shutdown", self.on_shutdown)
        util.event_listen(
            service,
            "client_connection_disconnected",
            self.client_connection_disconnected)

        self.peers_by_name = {}
        self.async_suite = self.service.async_suite

    @property
    def peers(self):
        return self.peers_by_name.values()

    def speak_rpc(self, rpc_reg):
        self.rpc_reg = self.rpc_reg.join(rpc_reg)
        self.service.speak_rpc(rpc_reg)

    @property
    def total_number_of_peers(self):
        return len(self.peernames)

    @property
    def number_of_connected_peers(self):
        return len([p for p in self.peers if p.connected])

    @property
    def number_of_disconnected_nonpartitioned_peers(self):
        return len([p for p in self.peers
                    if not p.connected and not p.is_partitioned])

    def _cluster_view(self):
        sorted_peers = sorted(self.peers, key=lambda peer: peer.name)
        log.info(
            "[%s] \nconnected: {%s}\ndisconnected: {%s}\npartitioned: {%s}" % (
                self.nodename,
                ", ".join([
                    peer.name
                    for peer in sorted_peers
                    if peer.connected]),
                ", ".join([
                    peer.name
                    for peer in sorted_peers
                    if not peer.connected and not peer.is_partitioned]),
                ", ".join([
                    peer.name for peer in sorted_peers
                    if not peer.connected and peer.is_partitioned])
            )
        )

    def broadcast_rpc_to_peers(self, msg):
        for peer in self.peers:
            if peer.connected:
                msg.send(peer.rpc_service)

    def _peer_error_received(self, peer, exception):
        self.dispatch.peer_exception_received(peer, exception)

    def before_listen(self, service):
        self.nodename = self.service.nodename
        assert self.nodename is not None

        for peername in self.peernames:
            if peername == self.nodename:
                continue
            elif peername > self.nodename:
                peer = ClientPeer(
                    self,
                    self.nodename,
                    self.service, peername
                )
                peer._start()
            else:
                peer = ServerPeer(
                    self,
                    self.nodename,
                    peername
                )

        self._cluster_view()

    def on_shutdown(self, service):
        log.info("Broadcast SHUTDOWN to peers")
        self.broadcast_rpc_to_peers(Shutdown())

    def client_connection_disconnected(
            self, server, client_connection, unexpected, message):
        if 'p2p_peer' in client_connection.memos:
            server_peer = client_connection.memos['p2p_peer']
            server_peer.disconnect(is_partition=unexpected)

    def _cmd_peers(self):
        peer_dict = dict(
            (name, False) for name in self.peernames
        )
        peer_dict.update(
            (peer.name, peer.connected) for peer in self.peers
        )
        return [
            "%s - %s" %
            (
                nodename,
                "this peer" if nodename == self.nodename
                else "connected" if peer_dict[nodename]
                else "disconnected"
            )
            for nodename in self.peernames
        ]


class PeerListener(util.EventListener):
    _dispatch_target = PeerRunner

    def new_peer(self, peer):
        pass

    def peer_connected(self, peer):
        pass

    def peer_partitioned(self, peer):
        pass

    def peer_disconnected(self, peer):
        pass

    def peer_exception_received(self, peer, exception):
        pass


class Peer(object):
    """generic 'peer' object that represents p2p operations on a socket.

    Base is agnostic of whether the connection is a client or server
    connection.

    """

    def __init__(self, handler, our_nodename, their_nodename):
        self.handler = handler
        self.async_suite = self.handler.async_suite
        self.our_nodename = our_nodename
        self.their_nodename = self.name = their_nodename
        self.memos = {}
        self.connected = False
        self.graceful_shutdown = False
        self.seen = False

        # if we're the bootstrap, assume nobody else has started
        # yet therefore not partitioned. Otherwise assume we don't
        # know their state, so partitioned.
        self.is_partitioned = not handler.is_bootstrap

    def _enter_pool(self):
        assert self.their_nodename not in self.handler.peers_by_name
        self.handler.peers_by_name[self.their_nodename] = self
        self.handler.dispatch.new_peer(self)

    def disconnect(self, is_partition):
        if self.graceful_shutdown:
            is_partition = False

        log.info(
            "[%s] received %spartitioned disconnect from [%s]",
            self.our_nodename,
            "" if is_partition else "non-",
            self.their_nodename)

        self.connected = False

        if is_partition:
            self.is_partitioned = True
            self.handler.dispatch.peer_partitioned(self)
        else:
            self.is_partitioned = False
            self.handler.dispatch.peer_disconnected(self)

        self.handler._cluster_view()


class ServerPeer(Peer):
    """peer that's connected via server.ClientConnection.

    """
    _init = False

    def __init__(
            self, handler, our_nodename, their_nodename):
        super(ServerPeer, self).__init__(handler, our_nodename, their_nodename)
        self.connected = False
        self._enter_pool()

    def _client_connected(self, client_connection, node_info):
        self.client_connection = client_connection
        self.client_connection.memos['p2p_peer'] = self
        self.connected = True
        self.graceful_shutdown = False
        self.is_partitioned = False
        self.rpc_service = self.client_connection.rpc_service

        # 1. if the peer connecting to us is "new" to the cluster,
        # we tell it which peers are partitioned via response
        if not self.seen:
            for name, connected, partitioned in node_info:
                if name != self.our_nodename:
                    self.handler.peers_by_name[name].is_partitioned = \
                        partitioned
                    self.handler.peers_by_name[name].connected = \
                        connected
            self.handler._cluster_view()
            self.seen = True

        log.info(
            "[%s] received peer connection from %s", self.our_nodename,
            self.their_nodename)
        self.handler.dispatch.peer_connected(self)
        self.handler._cluster_view()


class ClientPeer(Peer):
    """peer that's connected via client.Client.

    """

    def __init__(
            self, handler, our_nodename, service, their_nodename):
        super(ClientPeer, self).__init__(handler, our_nodename, their_nodename)
        self.client = GreenClient.from_config(
            service.config, service.servicename, their_nodename)
        self.rpc_service = self.client.speak_rpc(handler.rpc_reg)

        self.client.memos['p2p_peer'] = self

        util.event_listen(
            self.client, "client_connected", self._client_connected)
        util.event_listen(
            self.client, "client_disconnected", self._client_disconnected)

        self.connected = False
        self._enter_pool()

    def _start(self):
        log.info(
            "[%s] attempting to establish peer connection to [%s]",
            self.our_nodename, self.their_nodename
        )

        def status(success, exception, time):
            if not success:
                log.info(
                    "[%s] continuing to attempt connection to [%s], "
                    "%d seconds so far (can't connect: %s)",
                    self.our_nodename,
                    self.their_nodename,
                    time,
                    exception
                )

        self.client.connect_persistent(status_fn=status, notify_every=30)

    def _client_connected(self, client):
        response = SetAsPeer(
            self.our_nodename,
            [(peer.name, peer.connected, peer.is_partitioned)
             for peer in self.handler.peers]
        ).send(self.rpc_service)

        # 2. if the peer connecting to us is bringing us as "new" into
        # the cluster, we need to look here to see what it knows about
        # partitions
        if not self.seen:
            for name, connected, partitioned in response:
                if name != self.our_nodename:
                    self.handler.peers_by_name[name].is_partitioned = \
                        partitioned
                    self.handler.peers_by_name[name].connected = \
                        connected
            self.handler._cluster_view()
            self.seen = True

        log.info(
            "[%s] established peer connection to [%s] - %s",
            self.our_nodename, self.their_nodename, response)
        self.connected = True
        self.graceful_shutdown = False
        self.is_partitioned = False

        self.handler.dispatch.peer_connected(self)
        self.handler._cluster_view()

    def _client_disconnected(self, client, unexpected, message):
        self.disconnect(is_partition=unexpected)


class P2PConsoleCommands(object):
    name = 'p2p'

    def __init__(self, console):
        self.console = console
        self.console.register_cmds_from(self)
        self.rpc_service = console.speak_rpc(rpc_reg)

    def cmd_peers(self):
        """Display configured peers"""
        return Peers().send(self.rpc_service)

rpc_reg = rpc.RPCReg()


@rpc_reg.call()
class Peers(rpc.RPC):
    def receive_request(self, rpc, service_connection):
        p2p = service_connection.server.memos['p2p']
        if p2p is None:
            return ["%s - not p2p" % (service_connection.server.nodename, )]
        else:
            return p2p._cmd_peers()


@rpc_reg.call('name', 'peers')
class SetAsPeer(rpc.RPC):
    def receive_request(self, rpc, service_connection):
        p2p = service_connection.server.memos['p2p']
        server_peer = p2p.peers_by_name[self.name]
        server_peer._client_connected(service_connection, self.peers)

        return [(peer.name, peer.connected, peer.is_partitioned)
                for peer in p2p.peers]


@rpc_reg.call()
class IsClustered(rpc.RPC):

    def receive_request(self, rpc, service_connection):
        server = service_connection.server
        nodes = server.service_config['nodes']
        is_clustered = len(nodes) > 1 and \
            'p2p' in server.memos
        return is_clustered


class P2PRpcMixin(object):
    def receive_request(self, rpc, service_connection_or_client):
        try:
            peer = service_connection_or_client.memos['p2p_peer']
        except KeyError:
            raise exc.RPCError(
                "Received P2P RPC message from client that didn't "
                "send setaspeer: %s, %s" %
                (service_connection_or_client, self)
            )
        else:
            return self.receive_peer_request(rpc, peer)

    def receive_peer_request(self, rpc, peer):
        raise NotImplementedError()


class P2PRpc(P2PRpcMixin, rpc.RPC):
    pass


class P2PRpcEvent(P2PRpcMixin, rpc.RPCEvent):
    pass


@rpc_reg.call()
class Shutdown(P2PRpcEvent):
    def receive_peer_request(self, rpc, peer):
        peer.graceful_shutdown = True
