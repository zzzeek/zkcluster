import unittest
from zkcluster import rpc as _rpc
from zkcluster import exc
from zkcluster import util

rpc_reg = _rpc.RPCReg()
rpc_reg = rpc_reg.join(_rpc.rpc_reg)


@rpc_reg.call('x', 'y')
class DivideNumbers(_rpc.RPC):
    def receive_request(self, rpc, service):
        return self.x / self.y


@rpc_reg.call()
class ReportTarget(_rpc.RPC):
    def receive_request(self, rpc, service):
        return service.target_name


class RPCTest(unittest.TestCase):
    def _fixture(self, rpc_reg=rpc_reg):
        class Client(object):
            def __init__(self):
                self.async_suite = util.async_suite()
                self.rpc_service = _rpc.RPCService(rpc_reg, self)

            def send_message(self, message, need_response):
                self.server.message_received(message)

            def message_received(self, message):
                self.rpc_service.message_received(message)

            def connect(self, server):
                self.server = server
                server.client = self

        class Server(object):
            def __init__(self):
                self.async_suite = util.async_suite()
                self.rpc_service = _rpc.RPCService(rpc_reg, self)

            def send_message(self, message, need_response):
                self.client.message_received(message)

            def message_received(self, message):
                self.rpc_service.message_received(message)

        server = Server()
        client = Client()
        client.connect(server)

        return client

    def test_hello(self):
        client = self._fixture()
        msg = _rpc.HelloRPC("peer name", "some greeting")

        value = msg.send(client.rpc_service)
        self.assertEqual(value, 'Well some greeting to you too, peer name!')

    def test_error(self):
        client = self._fixture()
        msg = DivideNumbers(5, 0)

        self.assertRaisesRegexp(
            exc.RPCError,
            ".*by zero",
            msg.send, client.rpc_service
        )

        msg = DivideNumbers(4, 2)

        self.assertEqual(
            msg.send(client.rpc_service),
            2
        )

    def test_custom_encode_decode(self):
        class Widget(object):
            def __init__(self, x, y):
                self.x = x
                self.y = y

            def __eq__(self, other):
                return (
                    isinstance(other, Widget) and other.x == self.x and
                    other.y == self.y
                )

        rpc_reg = _rpc.RPCReg()
        rpc_reg.add_type(
            Widget, 'widget',
            lambda obj: (obj.x, obj.y), lambda struct: Widget(*struct))
        rpc_reg = rpc_reg.join(_rpc.rpc_reg)

        @rpc_reg.call('a')
        class WidgetThing(_rpc.RPC):
            def receive_request(self, rpc, service):
                return Widget(self.a.y, self.a.x)

        client = self._fixture(rpc_reg)
        msg = WidgetThing(Widget(5, 6))

        self.assertEqual(
            msg.send(client.rpc_service),
            Widget(6, 5)
        )

