import mock
import unittest

from zkcluster import config as _config
from zkcluster import server as _server
from zkcluster import rpc as _rpc
from zkcluster import util


class RemoteServerTest(unittest.TestCase):
    def _fixture(self):
        config = _config.Config()
        config.add_config_from_config_string("""
[zk_service_somename]
name: somename
nodes:
    node1 hostname=192.168.1.20:5084
auth_key: A
auth_secret: X
ssl_keyfile: keyfile.pem
ssl_certfile: cert.pem
monitor_cluster: rhel-cluster, single_pg_db

""")

        server = _server.RemoteServer(config, "somename", "node1")
        return server

    def test_serve_forever(self):
        server = self._fixture()
        with mock.patch.object(server.async_suite, "server") as streamserver:

            server.serve_forever()
        self.assertEqual(
            streamserver.mock_calls,
            [
                mock.call(
                    ('0.0.0.0', 5084),
                    server._connected,
                    certfile='cert.pem',
                    do_handshake_on_connect=True,
                    keyfile='keyfile.pem'
                ),
                mock.call().serve_forever()
            ]
        )


class ClientConnectionTest(unittest.TestCase):
    def _fixture(self):
        server = mock.Mock(
            _auth_key_secret=mock.Mock(
                side_effect=lambda key, secret: key == "A" and secret == "X"),
            rpc_reg=_rpc.RPCReg().join(_server.rpc_reg),
            async_suite=mock.Mock(
                lock=mock.MagicMock()
            )
        )

        socket = mock.Mock()
        address = "0.0.0.0"
        conn = _server.ClientConnection(server, socket, address)
        return conn

    def _authent_message(self, username, pw):
        return "Q(1, %s): %s" % (
            _server.Authenticate.rpc_name, util.json_dumps([username, pw])
        )

    def test_authenticate_success(self):
        conn = self._fixture()

        msg = self._authent_message("A", "X")
        self.assertTrue(
            conn._authenticate(msg)
        )
        self.assertEqual(
            conn.server._auth_key_secret.mock_calls,
            [mock.call('A', 'X')]
        )

    def test_authenticate_wrong_pw(self):
        conn = self._fixture()
        msg = self._authent_message("A", "Q")
        self.assertFalse(
            conn._authenticate(msg)
        )
        self.assertEqual(
            conn.server._auth_key_secret.mock_calls,
            [mock.call('A', 'Q')]
        )

    def test_authenticate_blank_pw(self):
        conn = self._fixture()
        msg = self._authent_message("A", "")
        self.assertFalse(
            conn._authenticate(msg)
        )
        self.assertEqual(
            conn.server._auth_key_secret.mock_calls,
            [mock.call('A', '')]
        )

    def test_authenticate_bad_msg(self):
        conn = self._fixture()
        self.assertFalse(
            conn._authenticate(
                'Q(1, %s): ["A", "CRAP!", "X"]' %
                _server.Authenticate.rpc_name)
        )
        self.assertEqual(
            conn.server._auth_key_secret.mock_calls,
            []
        )
        self.assertEqual(
            conn.socket.sendall.mock_calls,
            [
                mock.call(
                    'E(1, %s): __new__() takes exactly '
                    '3 arguments (4 given)\x00' %
                    (_server.Authenticate.rpc_name, )
                )
            ]
        )


