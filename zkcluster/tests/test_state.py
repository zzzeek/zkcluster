import mock
import unittest
from zkcluster.state import StateMachine


class StateMachineTest(unittest.TestCase):
    def _fixture(self):
        capture = mock.Mock()

        class SomeClass(object):
            state = StateMachine()
            START = state.START
            state1 = state.new("state1")
            state2 = state.new("state2")
            state3 = state.new("state3")
            state4 = state.new("state4")
            reentrant = state.new("reentrant")

            state.bounce(state1, state4)

            @state.transition(START, state2)
            def fn1(self, *arg, **kw):
                capture.fn1(*arg, **kw)

            @state.transition(state2, state3)
            @state.transition(state3, state1)
            def fn2(self, *arg, **kw):
                capture.fn2(*arg, **kw)

            @state.transition(state1, state2)
            def fn3(self, *arg, **kw):
                capture.fn3(*arg, **kw)

            @state.transition(START, state3)
            def fn4(self, from_, to, arg):
                if arg:
                    raise Exception("boom")

            @state.transition(state2, reentrant)
            def fn5(self, from_, to):
                # illegal
                self.state3.go(self)

            def __init__(self):
                self.state.init_instance(self)

        return SomeClass, capture

    def test_simple_transition(self):
        SomeClass, capture = self._fixture()

        obj = SomeClass()

        obj.state2.go(obj, 1, 2, foo='bar')
        obj.state3.go(obj, 3, )
        obj.state1.go(obj, "x", "y", x=1, y=2)

        self.assertEqual(
            capture.mock_calls,
            [

                mock.call.fn1(
                    obj.START, obj.state2, 1, 2, foo='bar'),
                mock.call.fn2(obj.state2, obj.state3, 3),
                mock.call.fn2(
                    obj.state3, obj.state1, 'x', 'y', y=2, x=1)
            ]
        )

    def test_exception_prevents_state_change(self):
        SomeClass, capture = self._fixture()

        obj = SomeClass()

        self.assertRaises(Exception, obj.state3.go, obj, True)
        self.assertTrue(obj.START.current(obj))

        obj.state3.go(obj, False)
        self.assertTrue(obj.state3.current(obj))

    def test_bounce(self):
        SomeClass, capture = self._fixture()
        obj = SomeClass()
        self.assertTrue(obj.START.current(obj))
        obj.state3.go(obj, False)
        obj.state1.go(obj)
        self.assertTrue(obj.state1.current(obj))
        obj.state4.go(obj)
        self.assertTrue(obj.state1.current(obj))

    def test_current(self):
        SomeClass, capture = self._fixture()

        obj = SomeClass()

        self.assertTrue(obj.START.current(obj))
        self.assertTrue(not obj.state2.current(obj))

        obj.state2.go(obj, 1, 2, foo='bar')

        self.assertTrue(obj.state2.current(obj))
        self.assertTrue(not obj.START.current(obj))

    def test_get(self):
        SomeClass, capture = self._fixture()

        obj = SomeClass()

        self.assertIs(
            obj.state.get(obj), obj.START
        )
        obj.state2.go(obj, 1, 2, foo='bar')
        self.assertIs(
            obj.state.get(obj), obj.state2
        )

    def test_illegal_transition(self):
        SomeClass, capture = self._fixture()

        obj = SomeClass()

        obj.state2.go(obj, 1, 2)

        with self.assertRaises(KeyError) as ke:
            obj.state1.go(obj)

        self.assertEqual(
            "Unknown transition: [state2]->[state1]",
            ke.exception.args[0]
        )

    def test_reeantrant_transition(self):
        SomeClass, capture = self._fixture()

        obj = SomeClass()

        obj.state2.go(obj)

        with self.assertRaises(AssertionError) as ae:
            obj.reentrant.go(obj)

        self.assertEqual(
            "reentrant state change call!",
            ae.exception.args[0]
        )


if __name__ == '__main__':
    unittest.main()
