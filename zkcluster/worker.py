from . import util
from . import p2p


class LeaderWorker(object):

    def work(self):
        raise NotImplementedError()

    @classmethod
    def receiver(cls, fn):
        fn_name = fn.__name__

        def reg_fn(self, *arg, **kw):
            if self.p2p:
                self.p2p.broadcast_rpc_to_peers(
                    LeaderWorkerBroadcast(self._worker_key, fn_name, arg, kw)
                )
            fn(self, _remote=False, *arg, **kw)
        reg_fn.rpc_receiver = fn
        return reg_fn

    @util.memoized_property
    def _worker_key(self):
        cls = self.__class__
        return "%s.%s" % (
            cls.__module__, cls.__name__
        )

    def _process_raft_state_change(self, raft, old_status, new_status):
        if new_status is raft.LEADER:
            self._start_work()
        else:
            self._stop_work()

    def _start_work(self):
        if self._continue_work:
            return
        self._continue_work = True
        self.block_working.release()

    def _stop_work(self):
        if not self._continue_work:
            return

        # loop will pause on keep_working
        self.block_working.acquire()

        # stop the work
        self._continue_work = False

    def work_for(self, server):
        self.server = server
        self.async_suite = server.async_suite

        server.memos["leaderworker_" + self._worker_key] = self

        self.block_working = self.async_suite.lock()
        self._continue_work = False
        self.block_working.acquire()
        self._worker_greenlet = self.async_suite.spawn(self.work)

        if 'raft' in server.memos:
            self.raft = server.memos['raft']
            self.p2p = self.raft.p2p

            if self.raft.is_leader:
                self._start_work()

            util.event_listen(
                self.raft, "state_changed", self._process_raft_state_change)

        else:
            self.raft = self.p2p = None
            self._start_work()

    @property
    def keep_working(self):
        """Return True if the worker should do work.

        Will block when the worker should pause.

        """

        self.block_working.acquire()
        try:
            return True
        finally:
            self.block_working.release()

    @property
    def continue_work(self):
        """"True if work should continue, False if should stop.

        Should only be accessed within the scope of keep_working.

        """
        return self._continue_work


@p2p.rpc_reg.call('worker_key', 'fn_name', 'arg', 'kw')
class LeaderWorkerBroadcast(p2p.P2PRpcEvent):
    def receive_peer_request(self, rpc, peer):

        leader_worker = peer.handler.service.memos[
            "leaderworker_" + self.worker_key]
        fn = getattr(leader_worker, self.fn_name)

        fn.rpc_receiver(leader_worker, _remote=True, *self.arg, **self.kw)
