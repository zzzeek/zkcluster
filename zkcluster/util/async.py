from .util import memoized_property
from .util import periodic_timer
from .. import exc
import contextlib
import time
import threading
import sys


@contextlib.contextmanager
def stop_on_keyinterrupt():
    try:
        yield
    except KeyboardInterrupt:
        pass


class Timeout(Exception):
    pass


class Waiter(object):
    """A result-holding object that will notify some form of callback
    when the result or exception is set."""

    __slots__ = (
        'suite', 'success', 'failure', 'thread', 'on_timeout',
        'timeout', 'result', 'exception', 'event'
    )

    NO_RESULT = object()

    def __init__(self, suite, timeout, on_timeout):
        self.suite = suite
        self.timeout = timeout
        self.on_timeout = on_timeout

        self.event = suite.event()
        self.result = self.NO_RESULT
        self.exception = None

    def set(self, value):
        self.result = value
        self.event.set()

    def set_exception(self, exception):
        self.exception = exception
        self.event.set()

    def wait_for_result(self):
        if not self.event.wait(self.timeout):
            if self.on_timeout:
                self.on_timeout()
            raise Timeout()
        elif self.result is not self.NO_RESULT:
            return self.result
        elif self.exception:
            raise self.exception

    def notify_on(self, success, failure):
        self.success = success
        self.failure = failure
        self.thread = self.suite.spawn(self._wait)

    def _wait(self):
        try:
            self.success(self.wait_for_result())
        except Exception as err:
            self.failure(err)


class AsyncSuite(object):
    @classmethod
    def get(cls, green=False):
        """Return a "suite" of async-sensitive utilities.

        This is a one-stop place to get a simple namespace with all those
        system calls that change w/ eventlet context.

        """
        if green:
            return _GreenAsyncSuite()
        else:
            return _ThreadedAsyncSuite()

    def create_connection(self, addr, timeout=None):
        return self.socket.create_connection(addr, timeout=timeout)

    def select_socket(self, socket, timeout):
        return self.select.select([socket], [], [], timeout)

    def waiter(self, timeout, on_timeout):
        return Waiter(self, timeout, on_timeout)

    def background_thread(self, fn):
        def go():
            with stop_on_keyinterrupt():
                fn()

        thread = self.thread(go)
        thread.daemon = True
        thread.start()
        return thread

    def receive_messages(
            self, socket, message_terminator, ping_handler, ping_interval):

        ping_timer = periodic_timer(ping_interval)
        ping_timer.reset(time.time())

        assert len(message_terminator) == 1

        buf = []
        while True:

            try:
                ready_to_read, ready_to_write, in_error = \
                    self.select_socket(socket, 1)
            except self.select_error as se:
                raise IOError("Select error: %s" % se)

            if ready_to_read:
                data = socket.recv(4096)
                if data == '':
                    raise IOError("no more data")
                while True:
                    mti = data.find(message_terminator)
                    if mti != -1:
                        buf.append(data[0:mti])
                        data = data[mti + 1:]
                        # print "receive message", ''.join(buf)
                        yield ''.join(buf)
                        buf[:] = []

                        if not data:
                            break
                    else:
                        buf.append(data)
                        break

            if ping_timer(time.time()):
                ping_handler()

            # allow pre-emption in case select() is broken
            self.sleep(.01)

    def event(self):
        raise NotImplementedError()

    def sleep(self):
        raise NotImplementedError()

    def spawn(self, fn):
        raise NotImplementedError()

    def queue(self):
        raise NotImplementedError()

    @memoized_property
    def socket(self):
        raise NotImplementedError()

    @memoized_property
    def select(self):
        raise NotImplementedError()

    @memoized_property
    def select_error(self):
        return self.select.error

async_suite = AsyncSuite.get


class _ThreadedAsyncSuite(AsyncSuite):
    def event(self):
        return threading.Event()

    def sleep(self, duration):
        return time.sleep(duration)

    def spawn(self, fn, *args, **kw):
        is_daemon = kw.pop('is_daemon', False)
        t = threading.Thread(target=fn, args=args, kwargs=kw)
        if is_daemon:
            t.daemon = True
        t.start()
        return t

    def thread(self, fn):
        return threading.Thread(target=fn)

    def lock(self):
        return threading.Lock()

    @memoized_property
    def queue(self):
        import Queue
        return Queue.Queue

    @memoized_property
    def socket(self):
        import socket
        return socket

    @memoized_property
    def select(self):
        import select
        return select

    def server(self, address, callback, **ssl_args):
        raise NotImplementedError()


class _GreenAsyncSuite(AsyncSuite):
    def __init__(self):
        import eventlet as _et
        import eventlet.event as _et_event
        import eventlet.green as _et_green
        self._eventlet = _et
        self._eventlet_green = _et_green
        self._eventlet_event = _et_event

    def spawn(self, fn, *args, **kw):
        # we need a traditional threading API, e.g.
        # join()
        is_daemon = kw.pop('is_daemon', False)
        thread = self.threading.Thread(target=fn, args=args, kwargs=kw)
        if is_daemon:
            thread.daemon = True
        thread.start()
        return thread

    @memoized_property
    def threading(self):
        from eventlet.green import threading
        return threading

    @memoized_property
    def queue(self):
        from eventlet import queue
        return queue.Queue

    def thread(self, fn):
        return self.threading.Thread(target=fn)

    @memoized_property
    def interrupted(self):
        import greenlet
        return greenlet.GreenletExit

    def event(self):
        from eventlet.green import threading
        return threading.Event()

    def sleep(self, duration):
        return self._eventlet.sleep(duration)

    def lock(self):
        return self.threading.Lock()

    @memoized_property
    def socket(self):
        return self._eventlet_green.socket

    @memoized_property
    def select(self):
        return self._eventlet_green.select

    def server(self, address, callback, **ssl_args):
        return _EventletServer(self, address, callback, **ssl_args)


class _EventletServer(object):
    def __init__(self, async_suite, address, callback, **ssl_args):
        if ssl_args:
            raise NotImplementedError("Ssl not supported yet")
        self.async_suite = async_suite
        self.address = address
        self.callback = callback

    def serve_forever(self, shutdown=None):
        server = self.async_suite._eventlet.listen(self.address)
        try:
            while True:
                try:
                    socket, address = server.accept()
                except OSError:
                    pass
                else:
                    self.async_suite.spawn(self.callback, socket, address)
        except (KeyboardInterrupt, SystemExit):
            if shutdown:
                shutdown()
